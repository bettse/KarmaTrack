#!/usr/bin/python

import urllib2
from pprint import pprint
import sqlite3
import time
import json
from sys import exit

USERNAME = "bettse"

conn = sqlite3.connect('reddit-' + USERNAME + '.db')

try:
    page = urllib2.urlopen("http://www.reddit.com/user/" + USERNAME + "/about.json")

    me = json.loads(page.read())
    linkkarma = me['data']['link_karma']
    commentkarma = me['data']['comment_karma']

    c = conn.cursor()

    date = int(time.time())
    query = "insert into karma (comment_karma, link_karma, date) values (?, ?, ?);"
    # Insert a row of data
    c.execute(query, (commentkarma, linkkarma, date))

    # Save (commit) the changes
    conn.commit()

    # We can also close the cursor if we are done with it
    c.close()

except urllib2.HTTPError:
    exit(0)


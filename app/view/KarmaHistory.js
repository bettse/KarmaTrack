Ext.require('Ext.chart.*');

Ext.define('app.view.KarmaHistory' ,{
    extend: 'Ext.chart.Chart',
    alias: 'widget.KarmaHistory',
    title: 'Karma over time',
    store: 'KarmaHistory',
    axes: [{
        type: 'Numeric',
        position: 'left',
        fields: ['comment_karma'],
        grid: true,
        label: {
            renderer: Ext.util.Format.numberRenderer('0,0')
        }
    }, {
        type: 'Time',
        position: 'bottom',
        dateFormat: 'M d',
        fields: ['date']
    }],
    series: [{
        type: 'line',
        axis: ['left', 'bottom'],
        yField: 'comment_karma',
        showMarkers: false,
        smooth: true,
        markerConfig: {
            type: 'cross',
            size: 4,
            radius: 4,
            'stroke-width': 0
        },
        xField: 'date',
        tips: {
            trackMouse: true,
            width: 50,
            height: 28,
            renderer: function(storeItem, item) {
                this.setTitle(storeItem.get('comment_karma'));
            }
        }
    }]
});

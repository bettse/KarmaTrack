Ext.define('app.model.KarmaHistory', {
    extend: 'Ext.data.Model',
    idProperty: 'date',
    fields: [
        { name: 'comment_karma', type: 'integer'},
        { name: 'link_karma', type: 'integer'},
        { name: 'date', type: 'date', dateFormat: 'timestamp'}
    ],
    proxy: {
        type: 'rest',
        url : 'karma.php'
    }
});

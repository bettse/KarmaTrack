Ext.define('app.controller.Karma', {
    extend: 'Ext.app.Controller',
    views: ['KarmaHistory'],
    models: ['KarmaHistory'],
    stores: ['KarmaHistory'],
    refs: [
    ],
    init: function() {
    },

    onLaunch: function(){
    }
});

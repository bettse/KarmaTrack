<?php
$yourfile = "reddit-bettse.db";
try{
    $dbh = new PDO("sqlite:$yourfile");
}catch(PDOException $e){
    echo $e->getMessage();
    echo "<br><br>Database -- NOT -- loaded successfully .. ";
    die( "<br><br>Query Closed !!! $error");
}
date_default_timezone_set('America/Los_Angeles');
$ideal = ($_REQUEST['ideal']) ? $_REQUEST['ideal'] : 50;
$query = "SELECT date, comment_karma, link_karma FROM karma ORDER BY date DESC";
$result = $dbh->query($query);

$data = array();
$i = 0;

$rows = $result->fetchAll(PDO::FETCH_CLASS);
$total = count($rows);
$skip = floor($total / $ideal);
#echo "Skip = $skip\n";
#
foreach ($rows as $row){
    if($skip == 0 || $i % $skip == 0){
        $data[] = $row;
        #echo "Added {$i}\n";
    }else{
        #echo "Skipped {$i}\n";
    }
    $i++;
}
echo json_encode($data);

?>

Ext.Loader.setConfig({
    enabled : true
});

Ext.require('Ext.chart.*');

Ext.application({
    name: 'app',
    controllers: ['Karma'],
    views: ['KarmaHistory'],
    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'vbox',
            width: 1300,
            items: [{
                xtype: 'panel',
                title: 'Comment Karma',
                layout: 'fit',
                width: 1200,
                padding: 10,
                items: [{
                    xtype: 'KarmaHistory',
                    width: 1400,
                    height: 400
                }]
            }]
        });
    }
});



